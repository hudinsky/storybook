import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

import Main from "./pages/Main";
import AboutWork from "./pages/AboutWork";
import AboutUs from "./pages/AboutUs";
import Examples from "./pages/Examples";
import Authors from "./pages/Authors";
import Contacts from "./pages/Contacts";
import PageScreen from "./components/Element/PageScreen";


class Content extends React.Component {
    render() {
        return (
            <div>
                <PageScreen id="main" className={"page-main"}>
                    <Main/>
                </PageScreen>

                <PageScreen id="aboutWork" className={"page-aboutWork"}>
                    <AboutWork/>
                </PageScreen>

                <PageScreen id="aboutUs" className={"page-aboutUs"}>
                    <AboutUs/>
                </PageScreen>

                <PageScreen id="examples" className={"page-myWork"}>
                    <Examples/>
                </PageScreen>

                <PageScreen id="authors" className={'page-authors'}>
                    <Authors/>
                </PageScreen>

                <PageScreen id="contacts" className={'page-contacts'}>
                    <Contacts/>
                </PageScreen>
            </div>
        );
    }
}

ReactDOM.render(
    <Content />,
    document.getElementById('root')
);
