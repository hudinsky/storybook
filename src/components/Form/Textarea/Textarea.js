import React from 'react';
import './Textarea.css';

class Textarea extends React.Component {
    render() {

        const {placeholder} = this.props;
        const isRoundedClass = this.props.isRounded ? 'is-rounded' : '';

        return (
            <textarea className={"textarea " + isRoundedClass} placeholder={placeholder} />
        );
    }
}
export default Textarea;
