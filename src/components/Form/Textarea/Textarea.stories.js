import React from 'react';

import Textarea from './Textarea';

export default {
    title: 'Form/Textarea',
    component: Textarea,
};

const Template = (args) => <Textarea {...args} />;

export const Default = Template.bind({});
Default.args = {
    isRounded: false,
    placeholder: 'enter text'
};

export const Rounded = Template.bind({});
Rounded.args = {
    isRounded: true,
    placeholder: 'enter text'
};

