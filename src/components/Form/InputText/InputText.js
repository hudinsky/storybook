import React from 'react';
import './InputText.css';

class InputText extends React.Component {
    render() {

        const {placeholder} = this.props;
        const isRoundedClass = this.props.isRounded ? 'is-rounded' : '';

        return (
            <input className={"input " + isRoundedClass} type="text" placeholder={placeholder} />
        );
    }
}
export default InputText;
