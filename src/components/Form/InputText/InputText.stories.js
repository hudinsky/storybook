import React from 'react';

import InputText from './InputText';

export default {
    title: 'Form/InputText',
    component: InputText,
};

const Template = (args) => <InputText {...args} />;

export const Default = Template.bind({});
Default.args = {
    isRounded: false,
    placeholder: 'enter text'
};

export const Rounded = Template.bind({});
Rounded.args = {
    isRounded: true,
    placeholder: 'enter text'
};

