import React from 'react';

import Button from './Button';

export default {
    title: 'Form/Button',
    component: Button,
};

const Template = (args) => <Button {...args} />;


export const Medium = Template.bind({});
Medium.args = {
    type: 'medium',
    children: 'Button'
};

export const Large = Template.bind({});
Large.args = {
    type: 'large',
    children: 'Button'
};

export const ExtraLarge = Template.bind({});
ExtraLarge.args = {
    type: 'extraLarge',
    children: 'Button'
};
