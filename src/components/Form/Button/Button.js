import React from 'react';
import './Button.css';

class Button extends React.Component {
    render() {
        const className = this.props.type ? 'button is-success is-rounded screenButton screenButton-' + this.props.type : 'screenButton';
        const onClick = this.props.onClick || (() => {});

        return (
            <button className={className} onClick={onClick}>{this.props.children}</button>
        );
    }
}
export default Button;
