import React from 'react';
import './ScreenText.css';

class ScreenText extends React.Component {
    render() {
        return (
            <div className={"screenText " + this.props.className} style={this.props.style}>{this.props.children}</div>
        );
    }
}
export default ScreenText;
