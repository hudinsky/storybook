import React from 'react';
import './PageScreen.css'

class Square extends React.Component {
    render() {
        return (
            <div id={this.props.id} className={this.props.className + ' pageScreen'}>
                {this.props.children}
            </div>
        );
    }
}
export default Square;
