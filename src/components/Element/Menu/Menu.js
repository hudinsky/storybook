import React from 'react';
import './Menu.css';

class Menu extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <div className="menu">
                {this.props.children}
            </div>
        );
    }
}
export default Menu;
