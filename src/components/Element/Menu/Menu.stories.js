import React from 'react';

import Menu from './Menu';
import MenuLink from "./MenuLink";

export default {
    title: 'Element/Menu',
    component: Menu,
    parameters: {
        backgrounds: {
            default: 'default',
            values: [
                { name: 'default', value: '#2D1B46' },
            ],
        },
    },
};

export const SingleItem = (args) => (
    <Menu {...args}>
        <MenuLink scrollTo="#">strona główna</MenuLink>
    </Menu>
);

export const MultipleItems = (args) => (
    <Menu {...args}>
        <MenuLink scrollTo="main">strona główna</MenuLink>
        <MenuLink scrollTo="aboutWork">nasze prace</MenuLink>
        <MenuLink scrollTo="aboutUs">o nas</MenuLink>
        <MenuLink scrollTo="contacts">kontakt</MenuLink>
    </Menu>
);

export const MultipleItemsWithActive = (args) => (
    <Menu {...args}>
        <MenuLink scrollTo="main">strona główna</MenuLink>
        <MenuLink scrollTo="aboutWork">nasze prace</MenuLink>
        <MenuLink scrollTo="aboutUs">o nas</MenuLink>
        <MenuLink scrollTo="contacts" active>kontakt</MenuLink>
    </Menu>
);
