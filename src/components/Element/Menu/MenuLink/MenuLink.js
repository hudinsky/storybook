import React from 'react';
import './MenuLink.css';
import PropTypes from "prop-types";

class MenuLink extends React.Component {
    render() {
        const activeClass = this.props.active ? 'menu-active' : '';

        const onClick = event => {
            event.preventDefault();
            document.getElementById(event.target.dataset.to).scrollIntoView();
        }

        return (
            <a className={"menu-link " + activeClass} data-to={this.props.scrollTo} onClick={onClick}>
                {this.props.children}
            </a>
        );
    }
}
export default MenuLink;

MenuLink.propTypes = {
    active: PropTypes.bool,
    children: PropTypes.string
};
