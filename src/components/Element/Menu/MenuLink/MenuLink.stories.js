import React from 'react';

import MenuLink from "./MenuLink";

export default {
    title: 'Element/MenuLink',
    component: MenuLink,
    parameters: {
        backgrounds: {
            default: 'default',
            values: [
                { name: 'default', value: '#2D1B46' },
            ],
        },
    },
};


const Template = (args) => <MenuLink {...args} />;

export const Default = Template.bind({});
Default.args = {
    active: false,
    children: 'Link'
};

export const Active = Template.bind({});
Active.args = {
    active: true,
    children: 'Link'
};

