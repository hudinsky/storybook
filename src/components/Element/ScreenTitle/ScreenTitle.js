import React from 'react';
import './ScreenTitle.css';
import PropTypes from "prop-types";

class ScreenTitle extends React.Component {
    render() {
        const type = this.props.type === 'primary' ? 'primary' : 'secondary';

        return (
            <div className={"screenTitle-text screenTitle-text-" + type}>
                <span>{this.props.children}</span>
            </div>
        );
    }
}
export default ScreenTitle;

ScreenTitle.propTypes = {
    type: PropTypes.oneOf(['primary', 'secondary']),
    // /**
    //  * What background color to use
    //  */
    // backgroundColor: PropTypes.string,
    // /**
    //  * How large should the button be?
    //  */
    // size: PropTypes.oneOf(['small', 'medium', 'large']),
    // /**
    //  * Button contents
    //  */
    // label: PropTypes.string.isRequired,
    // /**
    //  * Optional click handler
    //  */
    // onClick: PropTypes.func,
};
