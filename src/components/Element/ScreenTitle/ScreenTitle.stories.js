import React from 'react';

import ScreenTitle from './ScreenTitle';

export default {
    title: 'Element/ScreenTitle',
    component: ScreenTitle,
};

const Template = (args) => <ScreenTitle {...args} />;

export const Primary = Template.bind({});
Primary.args = {
    type: 'primary',
    children: 'Primary title'
};

export const Secondary = Template.bind({});
Secondary.args = {
    type: 'secondary',
    children: 'Secondary title'
};

