import React from 'react';

import Contact from "./Contact";

export default {
    title: 'Page/Contact',
    component: Contact,
    parameters: {
        backgrounds: {
            default: 'default',
            values: [
                { name: 'default', value: '#196C86' },
            ],
        },
    },
};

const Template = (args) => <Contact {...args} />;

export const Default = Template.bind({});
Default.args = {};
