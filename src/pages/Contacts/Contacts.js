import React from 'react';
import './Contacts.css';
import ScreenButton from "../../components/Form/Button";
import InputText from "../../components/Form/InputText";
import Textarea from "../../components/Form/Textarea";
import Contact from "./Contact";

class Contacts extends React.Component {
    render() {
        return (
            <div className="contacts">
                <div className="columns">
                    <div className="column contacts-title">kontakt</div>
                    <div className="column"/>
                </div>
                <div className="columns contacts-emailRow">
                    <div className="column contacts-emailColumn">
                        <InputText placeholder="e-mail" isRounded />
                    </div>
                    <div className="column">
                        <div className="contacts-rightColumnContent">
                            <div className="columns">
                                <div className="column">
                                    <Contact />
                                </div>
                                <div className="column">
                                    <Contact />
                                </div>
                                <div className="column">
                                    <Contact />
                                </div>
                                <div className="column">
                                    <Contact />
                                </div>
                                <div className="column">
                                    <Contact />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="columns">
                    <div className="column contacts-text">
                        <Textarea placeholder="treść" isRounded />
                    </div>
                    <div className="column">
                        <div className="contacts-address contacts-rightColumnContent">
                            <div>
                                adres: Kraków
                            </div>
                            <div>
                                telefon: +48 111 222 333
                            </div>
                        </div>
                    </div>
                </div>
                <div className="columns">
                    <div className="column contacts-submit">
                        <ScreenButton type="extraLarge" onClick={() => alert("Success")}>wyślij</ScreenButton>
                    </div>
                    <div className="column"/>
                </div>
            </div>
        );
    }
}
export default Contacts;
