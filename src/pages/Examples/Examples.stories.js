import React from 'react';

import './Examples.css';
import Examples from "./Examples";
import ExampleItem from "./ExampleItem";

export default {
    title: 'Page/Examples',
    component: Examples,
    parameters: {
        backgrounds: {
            default: 'default',
            values: [
                { name: 'default', value: '#196C86' },
            ],
        },
    },
};

export const One = () => (
    <div className={"examples"}>
        <ExampleItem title={"motion design"} text={"Lorem ipsum dolor sit Lorem ipsum dolor sit amet, consec"} />
    </div>
)

export const Two = () => (
    <div className={"examples"}>
        <ExampleItem title={"motion design"} text={"Lorem ipsum dolor sit Lorem ipsum dolor sit amet, consec"} />
        <ExampleItem title={"Ilustracja"} text={"Lorem ipsum dolor sit Lorem ipsum dolor sit amet, consec"} />
    </div>
)

export const Five = () => (
    <div className={"examples"}>
        <ExampleItem title={"motion design"} text={"Lorem ipsum dolor sit Lorem ipsum dolor sit amet, consec"} />
        <ExampleItem title={"Ilustracja"} text={"Lorem ipsum dolor sit Lorem ipsum dolor sit amet, consec"} />
        <ExampleItem title={"video i foto"} text={"Lorem ipsum dolor sit Lorem ipsum dolor sit amet, consec"} />
        <ExampleItem title={"motion design"} text={"Lorem ipsum dolor sit Lorem ipsum dolor sit amet, consec"} />
        <ExampleItem title={"Ilustracja"} text={"Lorem ipsum dolor sit Lorem ipsum dolor sit amet, consec"} />
    </div>
)
