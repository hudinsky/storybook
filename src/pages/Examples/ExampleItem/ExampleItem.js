import React from 'react';
import './ExampleItem.css';
import PropTypes from "prop-types";
import Author from "../../Authors/Author";

class ExampleItem extends React.Component {
    render() {
        return (
            <div className={"exampleItem"}>
                <div className={"exampleItem-square"} />
                <div className={"exampleItem-title"}>{this.props.title}</div>
                <div className={"exampleItem-text"}>{this.props.text}</div>
            </div>
        );
    }
}
export default ExampleItem;

Author.propTypes = {
    title: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
};
