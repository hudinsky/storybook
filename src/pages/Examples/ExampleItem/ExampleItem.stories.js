import React from 'react';

import ExampleItem from "./ExampleItem";

export default {
    title: 'Page/ExampleItem',
    component: ExampleItem,
    parameters: {
        backgrounds: {
            default: 'default',
            values: [
                { name: 'default', value: '#196C86' },
            ],
        },
    },
};

const Template = (args) => <ExampleItem {...args} />;

export const Default = Template.bind({});
Default.args = {
    title: 'motion design',
    text: 'Lorem ipsum dolor sit Lorem ipsum dolor sit amet, consec',
};
