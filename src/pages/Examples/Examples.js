import React from 'react';
import './Examples.css';
import ExampleItem from "./ExampleItem";

class Examples extends React.Component {
    render() {
        return (
            <div className={"examples"}>
                <ExampleItem title={"motion design"} text={"Lorem ipsum dolor sit Lorem ipsum dolor sit amet, consec"} />
                <ExampleItem title={"Ilustracja"} text={"Lorem ipsum dolor sit Lorem ipsum dolor sit amet, consec"} />
                <ExampleItem title={"video i foto"} text={"Lorem ipsum dolor sit Lorem ipsum dolor sit amet, consec"} />
            </div>
        );
    }
}
export default Examples;
