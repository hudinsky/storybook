import React from 'react';
import './Main.css';
import logo from "../../img/logo223x99.png";
import ScreenTitle from "../../components/Element/ScreenTitle";
import Menu from "../../components/Element/Menu";
import MenuLink from "../../components/Element/Menu/MenuLink";

class Main extends React.Component {
    render() {
        return (
            <div className="main">
                <img className="main-logo" src={logo} />
                <Menu>
                    <MenuLink scrollTo="main">strona główna</MenuLink>
                    <MenuLink scrollTo="aboutWork">nasze prace</MenuLink>
                    <MenuLink scrollTo="aboutUs">o nas</MenuLink>
                    <MenuLink scrollTo="contacts" active>kontakt</MenuLink>
                </Menu>
                <div className="main-title">
                    <ScreenTitle type="primary">
                        Lorem ipsum dolor<br/>
                        sit amet blabla
                    </ScreenTitle>
                </div>
            </div>
        );
    }
}
export default Main;
