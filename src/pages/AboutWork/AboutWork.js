import React from 'react';
import './AboutWork.css';
import ScreenTitle from "../../components/Element/ScreenTitle";
import ScreenText from "../../components/Element/ScreenText";
import ScreenButton from "../../components/Form/Button";

class AboutWork extends React.Component {
    render() {
        return (
            <div>
                <ScreenTitle type="secondary">
                    nace<br/>
                    prace
                </ScreenTitle>

                <ScreenText className="aboutWork-text">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                    sed do eiusmod tempor incididunt ut labore et dolore magna aliquam
                </ScreenText>

                <div className="aboutWork-button">
                    <ScreenButton type="medium">przejdź</ScreenButton>
                </div>
            </div>
        );
    }
}
export default AboutWork;
