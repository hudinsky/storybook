import React from 'react';
import './AboutUs.css';
import ScreenTitle from "../../components/Element/ScreenTitle";
import ScreenText from "../../components/Element/ScreenText";

class AboutUs extends React.Component {
    render() {
        return (
            <div>
                <ScreenTitle type="secondary">o nas</ScreenTitle>
                <ScreenText className="aboutUs-text">
                    Lorem ipsum dolor sit amet, consecLorem ipsum dolor sit amet, consectetuer adipisc- ing elit, sed
                    diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam
                </ScreenText>
            </div>
        );
    }
}
export default AboutUs;
