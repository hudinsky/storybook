import React from 'react';

import './Authors.css';
import Authors from "./Authors";
import Author from "./Author";

export default {
    title: 'Page/Authors',
    component: Authors,
};

export const One = () => (
    <div className={"authors"}>
        <Author title={"Kamil Wacławek"} text={"Lorem ipsum dolor sit amet, consecLorem ipsum dolor sit amet, consectetuer adipisc- ing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam"} />
    </div>
)

export const Two = () => (
    <div className={"authors"}>
        <Author title={"Kamil Wacławek"} text={"Lorem ipsum dolor sit amet, consecLorem ipsum dolor sit amet, consectetuer adipisc- ing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam"} />
        <Author title={"Kseniya Brekhava"} text={"Lorem ipsum dolor sit amet, consecLorem ipsum dolor sit amet, consectetuer adipisc- ing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam"} />
    </div>
)

export const Three = () => (
    <div className={"authors"}>
        <Author title={"Kamil Wacławek"} text={"Lorem ipsum dolor sit amet, consecLorem ipsum dolor sit amet, consectetuer adipisc- ing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam"} />
        <Author title={"Kseniya Brekhava"} text={"Lorem ipsum dolor sit amet, consecLorem ipsum dolor sit amet, consectetuer adipisc- ing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam"} />
        <Author title={"Kamil Wacławek"} text={"Lorem ipsum dolor sit amet, consecLorem ipsum dolor sit amet, consectetuer adipisc- ing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam"} />
    </div>
)

export const Four = () => (
    <div className={"authors"}>
        <Author title={"Kamil Wacławek"} text={"Lorem ipsum dolor sit amet, consecLorem ipsum dolor sit amet, consectetuer adipisc- ing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam"} />
        <Author title={"Kseniya Brekhava"} text={"Lorem ipsum dolor sit amet, consecLorem ipsum dolor sit amet, consectetuer adipisc- ing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam"} />
        <Author title={"Kamil Wacławek"} text={"Lorem ipsum dolor sit amet, consecLorem ipsum dolor sit amet, consectetuer adipisc- ing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam"} />
        <Author title={"Kseniya Brekhava"} text={"Lorem ipsum dolor sit amet, consecLorem ipsum dolor sit amet, consectetuer adipisc- ing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam"} />
    </div>
)
