import React from 'react';
import './Authors.css';
import Author from "./Author";

class Authors extends React.Component {
    render() {
        return (
            <div className={"authors"}>
                <Author title={"Kamil Wacławek"} text={"Lorem ipsum dolor sit amet, consecLorem ipsum dolor sit amet, consectetuer adipisc- ing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam"} />
                <Author title={"Kseniya Brekhava"} text={"Lorem ipsum dolor sit amet, consecLorem ipsum dolor sit amet, consectetuer adipisc- ing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam"} />
            </div>
        );
    }
}
export default Authors;
