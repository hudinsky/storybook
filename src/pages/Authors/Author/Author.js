import React from 'react';
import './Author.css';
import PropTypes from "prop-types";

class Author extends React.Component {
    render() {
        return (
            <div className={"author"}>
                <div className={"author-circle"} />
                <div className={"author-title"}>{this.props.title}</div>
                <div className={"author-text"}>{this.props.text}</div>
            </div>
        );
    }
}
export default Author;

Author.propTypes = {
    title: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
};
