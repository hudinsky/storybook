import React from 'react';

import Author from "./Author";

export default {
    title: 'Page/Author',
    component: Author,
};

const Template = (args) => <Author {...args} />;

export const Default = Template.bind({});
Default.args = {
    title: 'Author name',
    text: 'Lorem ipsum dolor sit amet, consecLorem ipsum dolor sit amet, consectetuer adipisc- ing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam',
};
