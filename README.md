# How to run project

## What should be installed

Node.js

## Before run
Install packages (only once)

### `npm install`

## Run

In the project directory, you should run:

### `npm start` to run website on http://localhost:3000/

In separate terminal window:

### `npm run storybook` to run storybook on http://localhost:6006/
